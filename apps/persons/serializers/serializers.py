from rest_framework import serializers
from persons.models.persons import PersonsModel

class PersonsSerializer(serializers.ModelSerializer):
  class Meta:
    model = PersonsModel
    fields = ['id', 'name', 'identity_number', 'email', 'date_of_birth', 'created_at', 'updated_at', 'deleted_at']
   