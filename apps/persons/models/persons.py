from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator, MinLengthValidator


class PersonsModel(models.Model):

    name_validator = [
        MinLengthValidator(3, "Name must be at least 3 characters"),
        MaxLengthValidator(100, "Name must be at most 100 characters")
    ]
    
    identity_number_validator = [
        MinLengthValidator(10, "Identity number must be at least 10 characters"),
        MaxLengthValidator(10, "Identity number must be at most 10 characters"),
    ]
    
    name = models.CharField(max_length=100, validators=name_validator)
    identity_number = models.CharField(max_length=10, unique=True, validators=identity_number_validator)
    email = models.EmailField(unique=True)
    date_of_birth = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    deleted_at = models.DateTimeField(null=True)

    def __str__(self):
        return self.name
      
    
    