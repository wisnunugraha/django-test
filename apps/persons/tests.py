from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from persons.models.persons import PersonsModel

class PersonsTestCase(TestCase):
  
    # Setup settings API Clien & Data
    def setUp(self):
        self.client = APIClient()
        self.user_profile_data = {
            "name": "John Doe",
            "identity_number": "1234567890",
            "email": "johndoe@example.com",
            "date_of_birth": "1990-01-01"
        }
        
    # Test create persons
    def test_create_persons(self):
        response = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(PersonsModel.objects.count(), 1)
        self.assertEqual(PersonsModel.objects.get().name, 'John Doe')
      
    # Test create persons name validation  
    def test_create_name_validation(self):
        self.user_profile_data['name'] = 'te'
        response = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
    # Test create persons identity number validation
    def test_create_identity_number_validation(self):
        self.user_profile_data['identity_number'] = '12345678901'
        response = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Test update persons and created persons first to get data id for update
    def test_update_persons(self):
        responses = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        person_id = responses.json()['result']['id']
        self.user_profile_data['name'] = 'Jane Doe'
        response = self.client.put(f'/api/v1/persons/{person_id}', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
    
    # Test update persons name with validation and created persons first to get data id for update
    def test_update_name_persons(self):
        responses = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        person_id = responses.json()['result']['id']
        self.user_profile_data['name'] = 'Ja'
        response = self.client.put(f'/api/v1/persons/{person_id}', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
           
    # Test delete persons and created persons first to get data id delete
    def test_delete_persons(self):
        responses = self.client.post('/api/v1/persons/', self.user_profile_data, format='json')
        person_id = responses.json()['result']['id']
        response = self.client.delete(f'/api/v1/persons/{person_id}/', self.user_profile_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

