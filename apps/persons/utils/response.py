

class ResponseData:
  @staticmethod
  def success(status, code, data):
    return {
      'status': status | True,
      'statusCode': code | 200,
      'result': data 
    }

  @staticmethod
  def error(message):
    return {
      'status': False,
      'statusCode': 400,
      'message': message
    }
    
  def validation_error(message):
    return {
      'status': False,
      'statusCode': 422,
      'message': message
    }