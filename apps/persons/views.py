from django.http import JsonResponse
from django.shortcuts import render

from persons.serializers.serializers import PersonsSerializer
from .models.persons import PersonsModel
from rest_framework.decorators import api_view
from rest_framework import generics, status
from rest_framework.response import Response
from .utils.response import ResponseData

class PersonsView:
  @api_view(['GET'])
  def index(request):
    queryset = PersonsModel.objects.all() 
    criteria = request.query_params
    if 'id' in criteria and criteria['id']:
        queryset = queryset.filter(id=criteria['id'])
    if 'name' in criteria and criteria['name']:
        queryset = queryset.filter(name__icontains=criteria['name'])
    if 'identity_number' in criteria and criteria['identity_number']:
        queryset = queryset.filter(identity_number=criteria['identity_number'])
    if 'email' in criteria and criteria['email']:
        queryset = queryset.filter(email__icontains=criteria['email'])
    if 'date_of_birth' in criteria and criteria['date_of_birth']:
        queryset = queryset.filter(date_of_birth=criteria['date_of_birth'])
    serializer = PersonsSerializer(queryset, many=True)
    return JsonResponse(ResponseData.success(True, 200, serializer.data), status=status.HTTP_201_CREATED)
  
  @api_view(['POST'])
  def create(request):
    serializer = PersonsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse(ResponseData.success(True, 200, serializer.data), status=status.HTTP_201_CREATED)
    else:
      return JsonResponse(ResponseData.validation_error(serializer.errors),status=status.HTTP_400_BAD_REQUEST)


  @api_view(['PUT'])
  def update(request, pk):
    try:
      queryset = PersonsModel.objects.get(pk=pk)
    except PersonsModel.DoesNotExist:
      return Response(ResponseData.error('Person not found'), status=status.HTTP_404_NOT_FOUND)
    serializer = PersonsSerializer(queryset, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return JsonResponse(ResponseData.success(True, 200, serializer.data), status=status.HTTP_202_ACCEPTED)
    return JsonResponse(ResponseData.validation_error(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    
    
  @api_view(['DELETE'])
  def delete(request, pk):
    try:
      queryset = PersonsModel.objects.get(pk=pk)
    except PersonsModel.DoesNotExist:
      return Response(ResponseData.error('Person not found'), status=status.HTTP_404_NOT_FOUND)
    queryset.delete()
    return JsonResponse(ResponseData.success(True, 200, 'Person deleted successfully'), status=status.HTTP_202_ACCEPTED)