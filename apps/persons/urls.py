
from django.urls import path
from .views import  PersonsView


urlpatterns = [
  path('persons', PersonsView.index, name='persons-list'),
  path('persons/', PersonsView.create , name='persons-create'),
  path('persons/<int:pk>', PersonsView.update , name='persons-update'),
  path('persons/<int:pk>/', PersonsView.delete , name='persons-delete'),
]
