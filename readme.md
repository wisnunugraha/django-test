## Authors

- [@wisnunugraha](https://www.gitlab.com/wisnunugraha)

## Task

Create API Persons
    [x] Persons List 
       [x] Persons Search multi params
       [x] Persons Detail  
       [X] Persons Delete


## Tech Stack

**Server:** Django, Django Rest Framework


**Databse:** Neon Postgresql

**Website:** Reactjs


## Usage/Examples

#API

```
pip install --no-cache-dir -r requirements.txt

```
Run Migrations and Migrate

```
Windows : python manage.py makemigrations persons && python manage.py migrate
Mac : python3 manage.py makemigrations persons && python3 manage.py migrate
```
Run Unit Test API CRUD

```
Windows : python manage.py test
Mac : python3 manage.py test
```


Run Server api

```
Windows : python manage.py runserver
Mac : python3 manage.py runserver
```

APP run on Port

```
APPS : http://localhost:8000
```


#Web

Run yarn

```
yarn
```

Web run on Port

```
WEB  : http://localhost:5173
```

#Docker

Run Docker Compose

```
docker compose up -d

APPS : http://localhost:8000
WEB  : http://localhost:5173
```

Import Postman File

```
Persons Data.postman_collection.json
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
