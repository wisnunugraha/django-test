import { useEffect, useState, createRef } from "react";
import axiosClient from "../api.jsx";
export default function persons() {
  const [message, setMessage] = useState(null);
  const [persons, setPersons] = useState([]);
  const [personsDetails, setPersonsDetails] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getPersons();
  }, []);

  const onDeleteClick = (persons) => {
    axiosClient.delete(`/persons/${persons.id}/`).then(() => {
      setPersonsDetails("");

      setMessage(`Persons ${persons.name} was deleted.`);
      getPersons();
    });
  };
  const onDetailsClick = (persons) => {
    setPersonsDetails("");
    const payload = {
      id: persons.id,
    };
    axiosClient
      .get("/persons", { params: payload })
      .then(({ data }) => {
        if (data.status === true) {
          setPersonsDetails(data.result[0]);
        } else {
          setPersonsDetails("");
        }
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const resetPersons = () => {
    getPersons();
  };

  const resetSearchPersons = () => {
    getPersons();
  };

  const nameRef = createRef();
  const numberRef = createRef();
  const emailRef = createRef();

  const nameAddRef = createRef();
  const numberAddRef = createRef();
  const emailAddRef = createRef();
  const yearAddRef = createRef();

  const getPersons = (ev) => {
    setPersonsDetails("");
    setPersons([]);
    const payload = {
      id: "",
      name: nameRef.current.value,
      identity_number: numberRef.current.value,
      email: emailRef.current.value,
    };
    setLoading(true);
    axiosClient
      .get("/persons", { params: payload })
      .then(({ data }) => {
        setLoading(false);
        if (data.status === true) {
          setPersons(data.result);
        } else {
          setPersons([]);
        }
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const getPersonsSearch = (ev) => {
    setPersonsDetails("");
    setPersons([]);
    ev.preventDefault();
    const payload = {
      id: "",
      name: nameRef.current.value,
      identity_number: numberRef.current.value,
      email: emailRef.current.value,
    };
    setLoading(true);
    axiosClient.get("/persons", { params: payload }).then(({ data }) => {
      if (data.status === true) {
        setPersons(data.result);
      } else {
        setPersons([]);
      }
      setLoading(false);
    });
  };

  const addPersons = (ev) => {
    ev.preventDefault();
    const payload = {
      name: nameAddRef.current.value,
      identity_number: numberAddRef.current.value,
      email: emailAddRef.current.value,
      date_of_birth: yearAddRef.current.value,
    };
    axiosClient
      .post("/persons/", payload)
      .then(({ data }) => {
        getPersons();
      })
      .catch((err) => {
        let test = [];
        if (err.response !== undefined) {
          Object.entries(err.response.data.message).forEach(([key, value]) =>
            value.forEach((error) => test.push(`${key} - ${error}`))
          );
          window.alert(test.join("\n"));
          getPersons(); 
        } else {
          getPersons();
        }
      })
      .finally(() => {
        getPersons();
      });
  };
  let i = 1;
  return (
    <div>
      <br />
      <br />
      <h4>Created Persons</h4>
      <form
        className="row "
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        onSubmit={(event) => addPersons(event)}
      >
        <div className="row">
          <div
            className="container row d-flex align-items-center justify-content-center"
            style={{ width: "100%" }}
          >
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputName">
                Name
              </label>
              <input
                type="text"
                className="form-control"
                id="inlineFormInputName"
                placeholder="Interstellar"
                ref={nameAddRef}
              />
            </div>
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputGroupUsername">
                Indetity Number
              </label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  id="inlineFormInputGroupUsername"
                  placeholder="2014"
                  ref={numberAddRef}
                />
              </div>
            </div>
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputGroupUsername">
                Email
              </label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  id="inlineFormInputGroupUsername"
                  placeholder="example@mail.com"
                  ref={emailAddRef}
                />
              </div>
            </div>
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputGroupUsername">
                Year
              </label>
              <div className="input-group">
                <input
                  type="date"
                  className="form-control"
                  id="inlineFormInputGroupUsername"
                  placeholder="example@mail.com"
                  ref={yearAddRef}
                />
              </div>
            </div>
            <div className="col-sm-2 ">
              <div className="input-group mt-3 ml-sm-1">
                <button type="submit" className="btn btn-success">
                  Add new
                </button>
              </div>
            </div>
            <div className="col-sm-1 ">
              <div className="input-group mt-3 ml-sm-1">
                <button type="reset" className="btn btn-danger">
                  Reset
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <br />
      <br />
      <br />
      <br />
      {message && (
        <div className="alert">
          <p>{message}</p>
        </div>
      )}

      {personsDetails && (
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">
              {"Name : "}
              {personsDetails.name} - {"Identity Number :"}
              {personsDetails.identity_number}
              {personsDetails.year}
            </h5>
            <br />
            <br />
            <p className="card-text">Email : {personsDetails.email}</p>
            <p className="card-text">Phone : {personsDetails.created_at}</p>
            <br />
            <br />
            <p className="card-text">
              &nbsp;
              <button
                type="reset"
                onClick={resetPersons}
                className="btn btn-danger"
              >
                Reset
              </button>
            </p>
          </div>
        </div>
      )}

      <br />
      <h5>List of persons</h5>
      <form
        className="row "
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        onSubmit={(event) => getPersonsSearch(event)}
      >
        <div className="row">
          <div
            className="container row d-flex align-items-center"
            style={{ width: "100%" }}
          >
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputName">
                Name
              </label>
              <input
                type="text"
                className="form-control"
                id="inlineFormInputName"
                placeholder="Interstellar"
                ref={nameRef}
              />
            </div>
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputGroupUsername">
                Indetity Number
              </label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  id="inlineFormInputGroupUsername"
                  placeholder="2014"
                  ref={numberRef}
                />
              </div>
            </div>
            <div className="col-sm-3 my-1">
              <label className="sr-only" for="inlineFormInputGroupUsername">
                Email
              </label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  id="inlineFormInputGroupUsername"
                  placeholder="example@mail.com"
                  ref={emailRef}
                />
              </div>
            </div>
            <div className="col-sm-1 ">
              <div className="input-group mt-3 ml-sm-5">
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
            <div className="col-sm-1 ">
              <div className="input-group mt-3 ml-sm-5">
                <button
                  type="reset"
                  onClick={resetSearchPersons}
                  className="btn btn-danger"
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>

      <div
        style={{
          alignItems: "center",
        }}
      ></div>
      <br />
      <br />
      <div className="card animated fadeInDown">
        {persons.length > 0 ? (
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Identity Number</th>
                <th>Email</th>
                <th>Date Of Birth</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {loading ? (
                <tr>
                  <td colSpan="5" className="text-center">
                    Loading...
                  </td>
                </tr>
              ) : (
                persons.map((u, index) => (
                  <tr key={u.id}>
                    <td>{index + 1}</td>
                    <td>{u.name}</td>
                    <td>{u.identity_number}</td>
                    <td>{u.email}</td>
                    <td>{u.date_of_birth}</td>
                    <td>
                      <button
                        className="btn btn-sm btn-success"
                        onClick={() => onDetailsClick(u)}
                        style={{
                          zIndex: 99999,
                        }}
                      >
                        Details Persons
                      </button>
                      &nbsp;
                      <button
                        className="btn btn-sm btn-warning"
                        onClick={(ev) => onDeleteClick(u)}
                      >
                        Delete Persons
                      </button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        ) : (
          <p>No persons found.</p>
        )}
      </div>
    </div>
  );
}
