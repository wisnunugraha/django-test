import { createBrowserRouter, Navigate } from "react-router-dom";
import DefaultPage from "./component/DefaultPage";
import Persons from "./views/Persons.jsx";
// import PersonsAdd from "./views/PersonsAdd.jsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <DefaultPage />,
    children: [
      {
        path: "/",
        element: <Navigate to="/dashboard" />,
      },
      {
        path: "/persons",
        element: <Persons />,
      },
      // {
      //   path: "/persons-add",
      //   element: <PersonsAdd />,
      // },
    ],
  },
]);

export default router;
