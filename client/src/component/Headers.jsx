import React from "react";

function Header() {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="/">
          Dashboard
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse d-flex justify-content-between"
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="/persons/">
                Persons
              </a>
            </li>
          </ul>
        </div>
      </nav>
      {/* <div>Menu Dashboard</div>

      <div>
        Hello, {user.fullname} &nbsp; &nbsp;
        <a onClick={onLogout} className="btn-logout" href="#">
          Logout
        </a>
      </div>
      <hr /> */}
    </header>
  );
}

export default Header;
