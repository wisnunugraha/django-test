import { Link, Navigate, Outlet, useNavigate } from "react-router-dom"
import Header from "./Headers";
import Persons from "../views/Persons.jsx";

export default function DefaultPage() {


  return (
    <div id="defaultLayout">
      <div className="content">
        <Header/> 
        <Persons />
      </div>
    </div>
  );
}
