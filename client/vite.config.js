import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      // Ensure that your project knows where to find Bootstrap
      // Adjust the path as necessary based on your project structure
      'bootstrap/dist/css/bootstrap.min.css': path.resolve(__dirname, './node_modules/bootstrap/dist/css/bootstrap.min.css'),
    },
  },
  
})
